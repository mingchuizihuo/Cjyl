package com.idea.cjyl.food.web.service;



import com.idea.cjyl.core.generic.GenericService;
import com.idea.cjyl.food.web.domain.pojo.DishesMeterial;
import com.idea.cjyl.food.web.domain.pojo.DishesMeterialExample;
public interface DishesMeterialService extends GenericService<DishesMeterial,Long,DishesMeterialExample> {


}
