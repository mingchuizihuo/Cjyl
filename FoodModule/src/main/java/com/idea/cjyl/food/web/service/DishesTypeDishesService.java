package com.idea.cjyl.food.web.service;


import com.idea.cjyl.core.generic.GenericService;
import com.idea.cjyl.food.web.domain.pojo.DishesTypeDishes;
import com.idea.cjyl.food.web.domain.pojo.DishesTypeDishesExample;

public interface DishesTypeDishesService extends GenericService<DishesTypeDishes,Long,DishesTypeDishesExample> {



}
