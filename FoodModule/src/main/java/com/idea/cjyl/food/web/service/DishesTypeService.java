package com.idea.cjyl.food.web.service;

import com.idea.cjyl.core.generic.GenericServiceLevel;
import com.idea.cjyl.food.web.domain.pojo.DishesType;
import com.idea.cjyl.food.web.domain.pojo.DishesTypeExample;

public interface DishesTypeService extends GenericServiceLevel<DishesType,Long,DishesTypeExample> {


}
