package com.idea.cjyl.food.web.service;

import com.idea.cjyl.core.generic.GenericServiceLevel;
import com.idea.cjyl.food.web.domain.pojo.Nutriment;
import com.idea.cjyl.food.web.domain.pojo.NutrimentExample;

public interface NutrimentService extends GenericServiceLevel<Nutriment,Long,NutrimentExample> {


}
