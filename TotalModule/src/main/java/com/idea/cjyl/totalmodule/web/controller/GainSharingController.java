package com.idea.cjyl.totalmodule.web.controller;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.idea.cjyl.core.common.ResultData;
import com.idea.cjyl.totalmodule.web.domain.pojo.OrganizationLogin;
import com.idea.cjyl.totalmodule.web.domain.vo.GainsharingVO;
import com.idea.cjyl.totalmodule.web.domain.vo.PageVO;
import com.idea.cjyl.totalmodule.web.service.GainsharingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by xiao on 2016/12/23.
 */
@Controller
@RequestMapping("/serve/gainsharing")
public class GainSharingController {
    @Autowired
    private GainsharingService gainsharingService;

    @ResponseBody
    @RequestMapping(value = "findAll",method = RequestMethod.GET)
    public ResultData getGainsharingList(Date startDate, Date endDate, Integer currentPage, Integer limit, HttpSession session) {

        OrganizationLogin organizationLogin = (OrganizationLogin) session.getAttribute("organizationLogin");
        if(currentPage==null){
            currentPage = 1;
        }
        if(limit==null){
            limit = 10;
        }
        List<GainsharingVO> gainsharingVOS = gainsharingService.getList(startDate, endDate,organizationLogin.getGainsharing());
        List<GainsharingVO> resultGainsharingVOS = new ArrayList<>();
        if(limit==1){
            resultGainsharingVOS.add(gainsharingVOS.get(currentPage-1));
            PageVO<GainsharingVO> page = new PageVO<>(limit,gainsharingVOS.size());
            page.setResult(resultGainsharingVOS);
            page.setPageNum(currentPage);
            page.setPageSize(limit);
            page.setTotal(gainsharingVOS.size());




            return ResultData.build().parseBean(page);
        }
        for(int i=(currentPage-1)*limit;i<limit ;i++){
            if(i>=gainsharingVOS.size()){
                break;
            }
            if(gainsharingVOS.get(i)==null){
                break;
            }else{
                resultGainsharingVOS.add(gainsharingVOS.get(i));
            }

        }

        PageVO<GainsharingVO> page = new PageVO<>(limit,gainsharingVOS.size());
        page.setResult(resultGainsharingVOS);
        page.setPageNum(currentPage);
        page.setPageSize(limit);
        page.setTotal(gainsharingVOS.size());




        return ResultData.build().parseBean(page);
    }

}
