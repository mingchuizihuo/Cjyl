package com.idea.cjyl.totalmodule.web.domain.pojo;

public class OrganizationLogin {
    private Long id;

    private String name;

    private Integer gainsharing;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Integer getGainsharing() {
        return gainsharing;
    }

    public void setGainsharing(Integer gainsharing) {
        this.gainsharing = gainsharing;
    }
}