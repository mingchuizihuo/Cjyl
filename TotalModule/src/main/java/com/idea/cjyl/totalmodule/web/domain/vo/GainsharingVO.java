package com.idea.cjyl.totalmodule.web.domain.vo;

import java.util.Date;

/**
 * Created by xiao on 2016/12/23.
 */
public class GainsharingVO {
    private Double terraceGainsharing;
    private Double organizationGainsharing;
    private int gainsharing;
    private String moneyType;

    private String closeAnAccountStr;

    @Override
    public String toString() {
        return "GainsharingVO{" +
                "terraceGainsharing=" + terraceGainsharing +
                ", organizationGainsharing=" + organizationGainsharing +
                ", gainsharing=" + gainsharing +
                ", moneyType='" + moneyType + '\'' +
                ", closeAnAccountStr='" + closeAnAccountStr + '\'' +
                '}';
    }

    public Double getTerraceGainsharing() {
        return terraceGainsharing;
    }

    public void setTerraceGainsharing(Double terraceGainsharing) {
        this.terraceGainsharing = terraceGainsharing;
    }

    public Double getOrganizationGainsharing() {
        return organizationGainsharing;
    }

    public void setOrganizationGainsharing(Double organizationGainsharing) {
        this.organizationGainsharing = organizationGainsharing;
    }

    public int getGainsharing() {
        return gainsharing;
    }

    public void setGainsharing(int gainsharing) {
        this.gainsharing = gainsharing;
    }

    public String getMoneyType() {
        return moneyType;
    }

    public void setMoneyType(String moneyType) {
        this.moneyType = moneyType;
    }

    public String getCloseAnAccountStr() {
        return closeAnAccountStr;
    }

    public void setCloseAnAccountStr(String closeAnAccountStr) {
        this.closeAnAccountStr = closeAnAccountStr;
    }
}
