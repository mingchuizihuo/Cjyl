package com.idea.cjyl.totalmodule.web.domain.vo;

import java.lang.reflect.Method;
import java.util.List;

/**
 * Created by xiao on 2016/12/23.
 */
public class PageVO<T> {

    /**
     * 页码，从1开始
     */
    private int pageNum;
    /**
     * 页面大小
     */
    private int pageSize;

    /**
     * 总数
     */
    private long total;
    /**
     * 总页数
     */
    private int pages;


    private List<T> result;


    public PageVO() {

    }
    public PageVO(int pageSize,int total){

        this.pages = (int)Math.ceil((double)total/pageSize);
    }

    @Override
    public String toString() {
        return "PageVO{" +
                "pageNum=" + pageNum +
                ", pageSize=" + pageSize +
                ", total=" + total +
                ", pages=" + pages +
                ", result=" + result +
                '}';
    }

    public int getPageNum() {

        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public List<T> getResult() {
        return result;
    }

    public void setResult(List<T> result) {
        this.result = result;
    }
}
