package com.idea.cjyl.totalmodule.web.service;

import com.idea.cjyl.totalmodule.web.domain.vo.GainsharingVO;

import java.util.Date;
import java.util.List;

/**
 * Created by xiao on 2016/12/23.
 */
public interface GainsharingService {
    /**
     * 查询收支分成
      * @param startDate
     * @param endDate
     * @param gainsharing
     * @return
     */
    public  List<GainsharingVO> getList(Date startDate, Date endDate,int gainsharing);
}
