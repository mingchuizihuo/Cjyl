package com.idea.cjyl.totalmodule.web.service.impl;

import com.idea.cjyl.core.util.DateConversion;
import com.idea.cjyl.totalmodule.web.dao.*;
import com.idea.cjyl.totalmodule.web.domain.pojo.*;
import com.idea.cjyl.totalmodule.web.domain.vo.GainsharingVO;
import com.idea.cjyl.totalmodule.web.globals.AnalysisConstant;
import com.idea.cjyl.totalmodule.web.service.GainsharingService;
import com.idea.cjyl.totalmodule.web.service.OlderAdmissionAndDischargeLogService;
import com.idea.cjyl.totalmodule.web.service.OlderInitialFeeService;
import com.idea.cjyl.totalmodule.web.service.OlderMonthChargeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by xiao on 2016/12/23.
 */
@Service
public class GainsharingServiceImpl implements GainsharingService {

    @Autowired
    private OlderAdmissionAndDischargeLogMapper olderAdmissionAndDischargeLogMapper;

    @Autowired
    private OlderInitialFeeMapper olderInitialFeeMapper;

    @Autowired
    private OlderMonthChargeMapper olderMonthChargeMapper;

    @Autowired
    private OlderCostMapper olderCostMapper;

    @Autowired
    private CheckInInitCostMapper checkInInitCostMapper;

    @Autowired
    private MonthChargeMapper monthChargeMapper;

    @Autowired
    private ServiceChargeMapper serviceChargeMapper;


    /**
     * 根据时间查询收支分成
      * @param startDate
     * @param endDate
     * @param gainsharing
     * @return
     */
    @Override
    public List<GainsharingVO> getList(Date startDate, Date endDate,int gainsharing) {
        if (startDate == null) {
            startDate = DateConversion.getNowDateFirstDay();
        }
        if (endDate == null) {
            endDate = DateConversion.getNowDateLastDay();
        }
        Date transit;
        if(startDate.getTime()>endDate.getTime()){
            transit =startDate;
            startDate = endDate;
            endDate = transit;
        }
        if(startDate.getTime()>endDate.getTime()){
            startDate = DateConversion.getNowDateFirstDay();
            endDate = DateConversion.getNowDateLastDay();
        }
        List<GainsharingVO> gainsharingVOS = new ArrayList<>();

        getOlderInitialFeeMoney(startDate,endDate,gainsharingVOS,gainsharing);

        getOlderMonthCharge(startDate,endDate,gainsharingVOS, gainsharing);

        getOlderCost(startDate,endDate,gainsharingVOS,gainsharing);

        return gainsharingVOS;
    }

    /**
     * 根据时间获取入院出院收入
     *
     * @param startDate
     * @param endDate
     * @return
     */
    public void getOlderAdmissionDischargeMoney(Date startDate, Date endDate,List<GainsharingVO> gainsharingVOS,int gainsharing) {
        OlderAdmissionAndDischargeLogExample example = new OlderAdmissionAndDischargeLogExample();
        example.createCriteria().andCheckInInitCostStateEqualTo(101l).andCloseAnAccountDateBetween(startDate, endDate);
        List<OlderAdmissionAndDischargeLog> olderAdmissionAndDischargeLogs = olderAdmissionAndDischargeLogMapper.selectByExample(example);


    }

    /**
     * 获取入住初始费用总收入
     *
     * @param startDate
     * @param endDate
     * @return
     */
    public void getOlderInitialFeeMoney(Date startDate, Date endDate,List<GainsharingVO> gainsharingVOS,int gainsharingO) {
        OlderInitialFeeExample olderInitialFeeExample = new OlderInitialFeeExample();
        olderInitialFeeExample.createCriteria().andCheckInInitCostStateEqualTo(101l).andCloseAnAccountDateBetween(startDate, endDate);
        List<OlderInitialFee> olderInitialFees = olderInitialFeeMapper.selectByExample(olderInitialFeeExample);

        long totalMoney = 0;//总钱数
        if (olderInitialFees.size() == 0) {


            for (OlderInitialFee olderInitialFee : olderInitialFees) {
                long checkInInitCostId = olderInitialFee.getCheckInInitCostId();
                CheckInInitCost checkInInitCost = checkInInitCostMapper.selectByPrimaryKey(checkInInitCostId);
                if (checkInInitCost != null) {
                    GainsharingVO gainsharingVO = new GainsharingVO();
                    Integer money = checkInInitCost.getMoney();
                    double gainsharing = gainsharingO/100.0;
                    if(olderInitialFee.getCloseAnAccountDate()==null || "".equals(olderInitialFee.getCloseAnAccountDate())){
                        olderInitialFee.setCloseAnAccountDate(new Date());
                    }
                    gainsharingVO.setGainsharing(gainsharingO);
                    gainsharingVO.setMoneyType("入住初始费");
                    gainsharingVO.setOrganizationGainsharing(money/gainsharing);
                    gainsharingVO.setTerraceGainsharing(money/(1-gainsharing));
                    gainsharingVO.setCloseAnAccountStr(DateConversion.dataformatYMD(olderInitialFee.getCloseAnAccountDate()));
                    gainsharingVOS.add(gainsharingVO);


                }

            }
        }
    }

    /**
     * 获取老人月收费总数
     *
     * @param startDate
     * @param endDate
     * @return
     */
    public void getOlderMonthCharge(Date startDate, Date endDate,List<GainsharingVO> gainsharingVOS ,int gainsharingO) {
        OlderMonthChargeExample olderMonthChargeExample = new OlderMonthChargeExample();
        olderMonthChargeExample.createCriteria().andOlderMonthChargeStateEqualTo(101l).andOlderMonthChargeEndDateBetween(startDate, endDate);
        List<OlderMonthCharge> olderMonthCharges = olderMonthChargeMapper.selectByExample(olderMonthChargeExample);

        if (olderMonthCharges.size() != 0) {
            for (OlderMonthCharge olderMonthCharge : olderMonthCharges) {
                long monthChargeId = olderMonthCharge.getMonthChargeId();
                MonthCharge monthCharge = monthChargeMapper.selectByPrimaryKey(monthChargeId);
                if (monthCharge != null) {
                    GainsharingVO gainsharingVO = new GainsharingVO();
                    Integer money = monthCharge.getTotal();
                    double gainsharing = gainsharingO/100.0;
                    if(olderMonthCharge.getOlderMonthChargeEndDate()==null || "".equals(olderMonthCharge.getOlderMonthChargeEndDate())){
                        olderMonthCharge.setOlderMonthChargeEndDate(new Date());
                    }
                    gainsharingVO.setGainsharing(gainsharingO);
                    gainsharingVO.setMoneyType("老人月收费");
                    gainsharingVO.setOrganizationGainsharing(money/gainsharing);
                    gainsharingVO.setTerraceGainsharing(money/(1-gainsharing));
                    gainsharingVO.setCloseAnAccountStr(DateConversion.dataformatYMD(olderMonthCharge.getOlderMonthChargeEndDate()));
                    gainsharingVOS.add(gainsharingVO);
                }

            }

        }


    }

    /**
     * 获取老人特殊服务总数
     * @param startDate
     * @param endDate
     * @return
     */
    public void getOlderCost(Date startDate,Date endDate,List<GainsharingVO> gainsharingVOS ,int gainsharingO){
        OlderCostExample olderCostExample = new OlderCostExample();
        olderCostExample.createCriteria().andServiceChargeStateEqualTo(101l).andCloseAnAccountDateBetween(startDate,endDate);
        List<OlderCost> olderCosts = olderCostMapper.selectByExample(olderCostExample);
        long totalMoney = 0;
        if(olderCosts.size()!=0) {


            for (OlderCost olderCost : olderCosts) {
                long serviceChargeId = olderCost.getServiceChargeId();
                ServiceCharge serviceCharge = serviceChargeMapper.selectByPrimaryKey(serviceChargeId);
                if (serviceCharge != null) {
                    GainsharingVO gainsharingVO = new GainsharingVO();
                    Integer money = serviceCharge.getMoney();
                    double gainsharing = gainsharingO/100.0;
                    if(olderCost.getCloseAnAccountDate()==null || "".equals(olderCost.getCloseAnAccountDate())){
                        olderCost.setCloseAnAccountDate(new Date());
                    }
                    gainsharingVO.setGainsharing(gainsharingO);
                    gainsharingVO.setMoneyType("老人特殊服务费");
                    gainsharingVO.setOrganizationGainsharing(money/gainsharing);
                    gainsharingVO.setTerraceGainsharing(money/(1-gainsharing));
                    gainsharingVO.setCloseAnAccountStr(DateConversion.dataformatYMD(olderCost.getCloseAnAccountDate()));
                    gainsharingVOS.add(gainsharingVO);
                }

            }
        }

    }
}
