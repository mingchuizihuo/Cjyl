<link href="${domainUrl}/assets/css/public.css" rel="stylesheet">
<link href="${domainUrl}/assets/css/foodWeek/foodWeek.css" rel="stylesheet">
<link href="${domainUrl}/assets/css/foodWeek/foodList.css" rel="stylesheet">
<script src="${domainUrl}/assets/js/foodWeek/foodList.js"></script>

<div class="overall" style="background-image: url(${domainUrl}/assets/images/backstage/bgg.jpg);">
<#--路径导航-->
    <div class="path">
        <ul>
            <li>您当前所在位置：</li>
            <li><a href="##">首页</a></li>
            <li><img src="${domainUrl}/assets/images/backstage/jiantou.png"></li>
            <li><a href="##">每周食譜</a></li>
            <li><img src="${domainUrl}/assets/images/backstage/jiantou.png"></li>
            <li class="txt_color">添加菜品</li>
            <div class="clearfix"></div>
        </ul>
    </div>

<div style="position: absolute; top: 0; left: 0; right: 0; bottom: 0; background-color: #DDD; opacity: .5; z-index:999; display: none;" id="aaaa">
</div>
<div id="box">
    <div id="test" ></div>
</div>
<div class="serve-banner">
    <an><input type="button" id="cao" class="input2" value="添加菜品" data-toggle="modal" data-target="#myModal" onclick="clearModal()" >
    </an>
</div>

<div class="main-module">
</div>

<#--添加服务模态框-->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="myModalLabel">
                        添加菜品
                    </h3>
                </div>
            <div class="modal-body">
                <table class="add">
                    <tr>
                        <td class="one">菜品名称</td>
                        <td class="two"><input type="text" id="dishesName" class="null"><input type="text" id="fid" style="display: none;"></td>
                    </tr>
                    <tr>
                        <td class="one">营养价值</td>
                        <td class="two">
                            <textarea id="dishesNutritiveValue">
                            </textarea>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default input3" data-dismiss="modal">取消</button>
                <button type="button" class="btn btn-primary input3" id="add-btn" onclick="add()">添加</button>
                <button type="button" class="btn btn-primary" id="update-btn" onclick="update()"
                        style="display: none;">修改
                </button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal -->
</div>
</div>

<#--&lt;#&ndash;添加模态框&ndash;&gt;-->
<#--<div class="modal fade" id="myModalCost" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">-->
    <#--<div class="modal-dialog">-->
        <#--<div class="modal-content">-->
            <#--<div class="modal-header">-->
                <#--<h3 class="modal-title  text-center" id="myModalLabel">-->
                    <#--添加计费模板-->
                <#--</h3>-->
            <#--</div>-->
            <#--<div class="modal-body">-->
                <#--<table class="add">-->
                    <#--<tr>-->
                        <#--<td>计费方式</td>-->
                        <#--<td><select class="serveCost" id="serveCost2"></select></td>-->
                    <#--</tr>-->
                    <#--<tr>-->
                        <#--<td>价格</td>-->
                        <#--<td><input type="text" id="costPirce2"></td>-->
                    <#--</tr>-->
                <#--</table>-->
            <#--</div>-->
            <#--<div class="modal-footer">-->
                <#--<button type="button" class="btn btn-default" data-dismiss="modal">取消</button>-->
                <#--<button type="button" class="btn btn-primary" id="add-cost"-->
                <#--">添加</button>-->
            <#--</div>-->
        <#--</div><!-- /.modal-content &ndash;&gt;-->
    <#--</div><!-- /.modal &ndash;&gt;-->
<#--</div>-->