<link href="${domainUrl}/assets/css/public.css" rel="stylesheet">
<link href="${domainUrl}/assets/css/foodWeek/foodWeek.css" rel="stylesheet">
<script src="${domainUrl}/assets/js/foodWeek/foodWeek.js"></script>


<div class="overall" style="background-image: url(${domainUrl}/assets/images/backstage/bgg.jpg);">
<#--路径导航-->
    <div class="path">
        <ul>
            <li>您当前所在位置：</li>
            <li><a href="##">首页</a></li>
            <li><img src="${domainUrl}/assets/images/backstage/jiantou.png"></li>
            <li><a href="##">每周食譜</a></li>
            <li><img src="${domainUrl}/assets/images/backstage/jiantou.png"></li>
            <li class="txt_color">每周食譜</li>
            <div class="clearfix"></div>
        </ul>
    </div>

<div class="content">
    <div class="serve-banner">
        <script>
            //    时间控件
            $('.data1').fdatepicker();
            var nowTemp = new Date();
            var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
        </script>
        <div id="date-input">
            <date>时间范围</date>
            <input type="text"  id="sdate"  class=" data1"  style="color:#333;">
            <date><input type="button" class="input" id="cao" value="搜索"></date>
        </div>
    </div>
    <!--本周食谱一周-->
    <div class="recipesList animated">
    </div>
    <!--食物弹出详情-->
    <div class="introduce" id="introduce">
        <div class="list-group-item">
            <h4 class="list-group-item-heading pull-left">营养价值:</h4>
            <input type="button" value="关闭" id="closeFoodDiv" class="btn pull-right" >
            <div class="clearfix"></div>
            <p class="list-group-item-text" id="dishesNutritiveValue"></p>
        </div>
    </div>
    <!--底部预览上一周食谱跟下一周食谱-->
    <div class="review">
        <button class="pull-right" id="nextDate">下一周<i class="glyphicon glyphicon-arrow-right"></i></button>
        <button class="pull-right" id="lastDate"><i class="glyphicon glyphicon-arrow-left"></i>上一周</button>
        <div class="clearfix"></div>
    </div>
</div>
</div>
<!--菜品选择-->
<div class="choose" style="display: none;">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">菜品选择</h4>
        </div>
        <div class="modal-body foodListShow">
            <#--<a class="">油条</a>-->
            <#--<a class="bb" >干煸大头菜心</a>-->
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn1" id="closeFoodAdd">关闭</button>
            <button type="button" class="btn btn1" value="111" id="foodOk" onclick="foodListAddUrl()">确认选择</button>
        </div>
    </div>
</div>
