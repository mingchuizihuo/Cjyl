<script>
    //    时间控件
    $('.data1').fdatepicker({
        format: 'yyyy-mm-dd',
    });
    var nowTemp = new Date();
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
</script>
<div class="tally">
    <h2>服务费用记账</h2>
    <div class="feiyong">
        <div class="box">
            <ul>
                <li><span>老人</span><input type="text" id="olderId"></li>
                <li><span>特殊服务</span><input type="text" id="serviceCharge"></li>
                <li><span>说明</span><input type="text" id="serviceChargeContext"> </li>
                <li><span>费用状态</span><input type="text" id="serviceChargeState"></li>
                <li><span>产生时间</span><input type="text" class="serviceChargeDate data1" id="sdate" ></li>
                <li><span>结算时间</span><input type="text" class="closeAnAccountDate data1" id="sdate" ></li>
                <li style="display: none"><input type="text" id="id"></li>
                <div class="clearfix"></div>
            </ul>
        </div>
    </div>
<#--增删改查按钮-->
    <div class="bottom">
        <button onclick="add()" id="addIs">确认添加</button>
        <button onclick="update()" id="updateIs">保存修改</button>
        <button class="modalClose" >取消</button>
    </div>
</div>
