<link href="${domainUrl}/assets/css/public.css" rel="stylesheet">
<link href="${domainUrl}/assets/css/report/arrearageNote.css" rel="stylesheet">
<link href="${domainUrl}/assets/css/report/gainsharing.css" rel="stylesheet">

<!--页面背景-->

<!---页面主体-->
<div class="overall" style="background-image: url(${domainUrl}/assets/images/backstage/bgg.jpg);">
<#--路径导航-->
    <div class="path">
        <ul>
            <li>您当前所在位置：</li>
            <li><a href="##">首页</a></li>
            <li><img src="${domainUrl}/assets/images/backstage/jiantou.png"></li>
            <li><a href="##">报表管理</a></li>
            <li><img src="${domainUrl}/assets/images/backstage/jiantou.png"></li>
            <li class="txt_color">收入分成</li>
            <div class="clearfix"></div>
        </ul>
    </div>
<#--查询导航-->
    <div class="query">
        <ul>
            <li>查询条件</li>
            <script>
                //    时间控件
                $('.data1').fdatepicker({
                    format: 'yyyy-mm-dd',
                });
                var nowTemp = new Date();
                var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
            </script>
            <li>起始时间<input type="text"  id="startDate"  class="data1 checkInDate"></li>
            <li>终止时间<input type="text"  id="endDate"  class="data1 checkInDate"></li>
            <button class="pull-right btn-style"onclick="findAllSacer(1)"><i class="glyphicon glyphicon-search"></i>查询</button>
            <div class="clearfix"></div>
        </ul>
    </div>

    <div class="arrearageNote">
        <h2>收入分成月度表</h2>
        <div class="aa">
            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>月份时间</th>
                    <th>平台收入（元）</th>
                    <th>实际收入（元）</th>
                    <th>收入分成（%）</th>
                    <th>费用类型</th>
                </tr>
                </thead>
                <tbody id="gainsharing">
                </tbody>
            </table>
        </div>
    <#--页码-->
        <div class="tcdPageCode" style="display: block">
        </div>

    </div>

</div>


<script src="${domainUrl}/assets/js/TotalModule/report/gainsharing.js"></script>